#!/bin/bash
# Inspiré par :
# https://vincent.composieux.fr/article/installer-configurer-et-renouveller-automatiquement-un-certificat-ssl-let-s-encrypt
# Création    : 09-02-2016
# Mise à jour : 04-02-2017
# -------------------

# Fichier de configuration
source /etc/letsencrypt/majle.config.sh

LE_PATH='/opt/letsencrypt'

#Limite d'expiration
EXP_LIMIT=30;

# Le log
LOG='LetsEncryptMaj'

# On initialise une variable pour voir si l'on doit relancer Apache…
RESTART=0

logger -t $LOG  '*Début de la mise à jour des certificats*'

for CONFIG_FILE in $CONFIG_FILES
do
if [ ! -f $CONFIG_FILE ]; then
        echo "[ERREUR] fichier de configuration non trouvé : $CONFIG_FILE"
	logger -t $LOG "ERREUR : Fichier de conf non trouvé $CONFIG_FILE'"
        exit 1;
fi

DOMAIN=`grep "^\s*domains" $CONFIG_FILE | sed "s/^\s*domains\s*=\s*//" | sed 's/(\s*)\|,.*$//'`
DOMAIN=${DOMAIN%% } # On ôte l'espace final…
CERT_FILE="/etc/letsencrypt/live/$DOMAIN/fullchain.pem"

logger -t $LOG "Cerificat $CERT_FILE"

if [ ! -f $CERT_FILE ]; then
	echo "[ERREUR] fichier du certificat non trouvé pour le domaine $DOMAIN."
	logger -t $LOG "ERREUR : Cerificat non trouvé $DOMAIN'"
fi

DATE_NOW=$(date -d "now" +%s)
EXP_DATE=$(date -d "`openssl x509 -in $CERT_FILE -text -noout | grep "Not After" | cut -c 25-`" +%s)
EXP_DAYS=$(echo \( $EXP_DATE - $DATE_NOW \) / 86400 |bc)
EXP_DATE_HUMAIN=$(date -d "@$EXP_DATE" +"%x à %X")

echo "⇒ Le certificat du domaine **$DOMAIN** expire le $EXP_DATE_HUMAIN."
logger -t $LOG "Date expiration : $EXP_DATE (dans $EXP_DAYS)"

if [ "$EXP_DAYS" -gt "$EXP_LIMIT" ] ; then
	echo "     Le certificat est à jour, pas besoin de le renouveler (expire dans $EXP_DAYS jours)."
	echo " "
	logger -t $LOG "Pas besoin de le renouveler"
else
	echo "     Le certificat pour $DOMAIN va bientôt expirer (dans $EXP_DAYS jours). On démarre le script de renouvelement (webroot)…"
	logger -t $LOG "Renouvelement..."
        $LE_PATH/certbot-auto certonly --text --non-interactive --renew-by-default --config $CONFIG_FILE --authenticator apache |logger -t $LOG
	# Ne pas oublier de redémarer les services…
	RESTART=1
	echo "     Processus de renouvellement du certificat du domaine $DOMAIN terminé"
	echo " "
	logger -t $LOG "C'est fini pour $DOMAIN"
fi

done
if [ $RESTART == 1 ] ; then
	echo "* On doit redémarrer $SERVICES"
	for SRV in $SERVICES
	do
		logger -t $LOG "On relance $SRV"
		/usr/sbin/service $SRV reload |logger -t $LOG
	done
fi

logger -t $LOG "C'est tout fini !"
exit 0;
