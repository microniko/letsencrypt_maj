Script écrit en Bash pour mettre à jour les certificats Let's Encrypt
=====================================================================

Cloner le dépot
---------------
Dans `/usr/local/sbin/` :
``` 
cd /usr/local/sbin/
git clone https://framagit.org/microniko/letsencrypt_maj.git
```
Configuration
-------------
Copier le fichier `majle.config.sh` dans le dossier `/etc/letsencrypt/` et le modifier selon les besoins.

Programation
------------
Copier le fichier `cron-letsencrypt` dans le dossier `/etc/cron.d` (par défaut, lancement à 3h42).
